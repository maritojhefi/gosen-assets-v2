/*! Gosen.io. */
!(function (i) {
    var s = i("#ajax-modal"),
        o = ".input-switch, .select, .input-checkbox, .input-bordered";/**/
    (_button_submit = "button[type=submit]");
        0 < (y = i("form#regalo_config")).length &&
            ((c = y.find(_button_submit)),
            y.find(o).on("keyup change", function () {
                btn_actived(c);
            }),
            ajax_form_submit(y, !1));

    var g,
        p,
        k,
        v,
        b; 
		
		 i(document).on("click", ".gift-view-ajax-action", function (t) {
            t.preventDefault();
            var e = i(this),
                a = e.data(),
                n = null,
                o = a.action,
                t = a.gift;
            e.parents(".gift-action").find(".toggle-tigger").add(".toggle-class").removeClass("active"),
                "overview" == o && "undefined" != typeof regalo_view_action_url && (n = regalo_view_action_url),
                null !== n && t
                    ? ((a._token = csrf_token),
                      i
                          .post(n, a)
                          .done(function (t) {
                              var e;
                              void 0 !== t.modal && t.modal
                                  ? (s.html(t.modal), init_inside_modal(), 0 < s.children(".modal").length && s.children(".modal").modal("show"))
                                  : t.message && ((e = t.icon || "ti ti-info-alt"), show_toast(t.msg, t.message, e));
                          })
                          .fail(function (t, e, a) {
                              show_toast("error", "Something is wrong!\n" + a, "ti ti-alert"), _log(t, e, a);
                          }))
                    : show_toast("info", "Nothing to proceed!", "ti ti-info-alt");
        }); 
})(jQuery);
