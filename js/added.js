$(document).ready(function() {
    let clock;

    // Grab the current date
    let currentDate = new Date();

    // Target future date/24 hour time/Timezone
    let targetDate = moment.tz("2023-02-14 09:00", "America/La_Paz");

    // Calculate the difference in seconds between the future and current date
    let diff = targetDate / 1000 - currentDate.getTime() / 1000;

    if (diff <= 0) {
        // If remaining countdown is 0
        clock = $(".clock").FlipClock(0, {
            clockFace: "DailyCounter",
            countdown: true,
            autostart: false
        });

        console.log("Date has already passed!");

    } else {
        // Run countdown timer
        clock = $(".clock").FlipClock(diff, {
            clockFace: "DailyCounter",
            countdown: true,
            callbacks: {
                stop: function() {
                    console.log("Timer has ended!");
                }
            }
        });



        // Check when timer reaches 0, then stop at 0
        setTimeout(function() {
            checktime();
        }, 1000);

        function checktime() {
            t = clock.getTime();
            if (t <= 0) {
                clock.setTime(0);
            }
            setTimeout(function() {
                checktime();
            }, 1000);
        }
    }
});



$(document).ready(function() {


    let clock2;

    // Grab the current date
    let currentDate = new Date();

    // Target future date/24 hour time/Timezone
    let targetDate = moment.tz("2023-02-14 09:00", "America/La_Paz");

    // Calculate the difference in seconds between the future and current date
    let diff = targetDate / 1000 - currentDate.getTime() / 1000;

    if (diff <= 0) {
        // If remaining countdown is 0
        clock2 = $(".clock2").FlipClock(0, {
            clockFace: "DailyCounter",
            countdown: true,
            autostart: false
        });

        console.log("Date has already passed!");

    } else {
        // Run countdown timer
        clock2 = $(".clock2").FlipClock(diff, {
            clockFace: "DailyCounter",
            countdown: true,
            callbacks: {
                stop: function() {
                    console.log("Timer has ended!");
                }
            }
        });



        // Check when timer reaches 0, then stop at 0
        setTimeout(function() {
            checktime();
        }, 1000);

        function checktime() {
            t = clock2.getTime();
            if (t <= 0) {
                clock2.setTime(0);
            }
            setTimeout(function() {
                checktime();
            }, 1000);
        }
    }
});
TweenMax.set(".play-circle-01", {
    rotation: 90,
    transformOrigin: "center"
});


TweenMax.set(".play-circle-02", {
    rotation: -90,
    transformOrigin: "center"
});


TweenMax.set(".play-perspective", {
    xPercent: 6.5,
    scale: .175,
    transformOrigin: "center",
    perspective: 1
});


TweenMax.set(".play-video", {
    visibility: "hidden",
    opacity: 0
});


TweenMax.set(".play-triangle", {
    transformOrigin: "left center",
    transformStyle: "preserve-3d",
    rotationY: 10,
    scaleX: 2
});


const rotateTL = new TimelineMax({
    paused: true
}).
to(".play-circle-01", .7, {
        opacity: .1,
        rotation: '+=360',
        strokeDasharray: "456 456",
        ease: Power1.easeInOut
    },
    0).
to(".play-circle-02", .7, {
        opacity: .1,
        rotation: '-=360',
        strokeDasharray: "411 411",
        ease: Power1.easeInOut
    },
    0);

const openTL = new TimelineMax({
    paused: true
}).
to(".play-backdrop", 1, {
        opacity: .95,
        visibility: "visible",
        ease: Power2.easeInOut
    },
    0).
to(".play-close", 1, {
        opacity: 1,
        ease: Power2.easeInOut
    },
    0).
to(".play-perspective", 1, {
        xPercent: 0,
        scale: 1,
        ease: Power2.easeInOut
    },
    0).
to(".play-triangle", 1, {
        scaleX: 1,
        ease: ExpoScaleEase.config(2, 1, Power2.easeInOut)
    },
    0).
to(".play-triangle", 1, {
        rotationY: 0,
        ease: ExpoScaleEase.config(10, .01, Power2.easeInOut)
    },
    0).
to(".play-video", 1, {
        visibility: "visible",
        opacity: 1
    },
    .5);


const button = document.querySelector(".play-button");
const backdrop = document.querySelector(".play-backdrop");
const close = document.querySelector(".play-close");

button.addEventListener("mouseover", () => rotateTL.play());
button.addEventListener("mouseleave", () => rotateTL.reverse());
button.addEventListener("click", () => openTL.play());
backdrop.addEventListener("click", () => openTL.reverse());
close.addEventListener("click", e => {
    e.stopPropagation();
    openTL.reverse();
});
//# sourceURL=pen.js